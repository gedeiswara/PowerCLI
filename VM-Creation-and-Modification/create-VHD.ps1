Write-Output "Usage : .\create-VHD.ps1 "VM-NAME" (DiskSize in GB) (DiskCount)"
Write-Host "Example : .\create-VHD.ps1 "xxx" 20 1"
$i = 1
$vm = Get-VM $args[0]
do 
{
New-Harddisk -VM $vm -CapacityGB $args[1] -StorageFormat Thin
$i++
}
while ($i -lt $args[2])